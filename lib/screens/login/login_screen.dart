import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/welcome_page/welcome_page.dart';
import 'package:awas360_mobile/widgets/primary_button/primary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

bool _obscurePassword = true;

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bodyWidget(context),
    );
  }

  Widget bodyWidget(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            tileMode: TileMode.clamp,
            colors: [AppColors.gradientStart, AppColors.gradientEnd]),
      ),
      child: Center(
          child: SingleChildScrollView(
        child: Column(children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Container(child: Image.asset('assets/icon/ic_logo.png')),
          Container(
              child: Image.asset(
            'assets/icon/ic_awas.png',
            height: 70,
          )),
          SizedBox(
            height: 50.0,
          ),
          FormBuilder(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 40, right: 40),
                  child: FormBuilderTextField(
                    cursorColor: AppColors.primaryLightkColor,
                    attribute: 'email',
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: 'Email',
                        hintStyle: TextStyle(color: Colors.white54),
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.cyanAccent,
                            )),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25.0),
                            borderSide: BorderSide(
                              color: Colors.white,
                            ))),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.only(left: 40, right: 40),
                  child: FormBuilderTextField(
                    attribute: 'password',
                    textAlign: TextAlign.center,
                    obscureText: _obscurePassword,
                    maxLines: 1,
                    style: TextStyle(color: Colors.white),
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                      hintText: 'Password',
                      hintStyle: TextStyle(color: Colors.white54),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(
                            color: Colors.cyanAccent,
                          )),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(
                            color: Colors.white,
                          )),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: 30),
                  alignment: Alignment.topRight,
                  child: FlatButton(
                    onPressed: () {},
                    child: Text(
                      'Lupa Password?',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        color: Colors.white70,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40, right: 40, top: 20),
                  child: PrimaryButton(
                    text: 'LOGIN',
                    textStyle:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    textColor: AppColors.primaryColor,
                    color: Colors.white,
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WelcomePageScreen())),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    '- Masuk Dengan -',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.white70,
                      fontSize: 16,
                    ),
                  ),
                ),
                Container(
                    child: Image.asset(
                  'assets/icon/ic_google2.png',
                  height: 70,
                )),
              ],
            ),
          ),
        ]),
      )),
    );
  }
}
