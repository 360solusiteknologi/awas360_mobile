import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:flutter/material.dart';

class Walkthrougth extends StatelessWidget {
  final String title;
  final String subtitle;
  final String imageVector;
  Walkthrougth({Key key, @required this.title, this.imageVector, this.subtitle})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            tileMode: TileMode.clamp,
            colors: [AppColors.gradientStart, AppColors.gradientEnd]),
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Container(
          margin: EdgeInsets.only(top: 50),
          child: Column(children: <Widget>[
            Image.asset(
              imageVector,
            ),
            Container(
                margin: EdgeInsets.only(top: 60),
                child: Text(
                  title,
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.bold),
                )),
            Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Text(
                  subtitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ))
          ])),
    );
  }
}
