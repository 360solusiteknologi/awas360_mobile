import 'package:awas360_mobile/screens/login/login_screen.dart';
import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/walkthrough/walkthrough.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';

class WalkthroughScreen extends StatefulWidget {
  @override
  _WalkthroughScreenState createState() => _WalkthroughScreenState();
}

class _WalkthroughScreenState extends State<WalkthroughScreen> {
  int currentIndexPage;
  int pageLength;

  @override
  void initState() {
    currentIndexPage = 0;
    pageLength = 3;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        PageView(
          children: <Widget>[
            Walkthrougth(
              imageVector: 'assets/vector/vector_w1.png',
              title: "Walkthrough One",
              subtitle:
                  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.',
            ),
            Walkthrougth(
              imageVector: 'assets/vector/vector_w2.png',
              title: "Walkthrough Two",
              subtitle:
                  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.',
            ),
            Walkthrougth(
              imageVector: 'assets/vector/vector_w3.png',
              title: "Walkthrough Tree",
              subtitle:
                  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.',
            ),
          ],
          onPageChanged: (value) {
            setState(() => currentIndexPage = value);
          },
        ),
        Column(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(bottom: 30),
                child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: DotsIndicator(
                    dotsCount: pageLength,
                    position: currentIndexPage.toDouble(),
                    decorator: DotsDecorator(
                        color: AppColors.primaryColor,
                        activeColor: Colors.white),
                  ),
                ),
              ),
            ),
            currentIndexPage == 2
                ? InkWell(
                    onTap: () => Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => LoginScreen())),
                    child: Container(
                      margin: EdgeInsets.only(right: 20, bottom: 20),
                      alignment: Alignment.bottomRight,
                      child: Text('Selesai',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 18,
                          )),
                    ),
                  )
                : Container(
                    margin: EdgeInsets.only(right: 20, bottom: 20),
                    alignment: Alignment.bottomRight,
                    child: Text('',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 18,
                        )),
                  )
          ],
        ),
      ],
    ));
  }
}
