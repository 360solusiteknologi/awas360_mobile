import 'package:awas360_mobile/screens/theme/curve_shape.dart';
import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/wizard/emergency_call.dart';
import 'package:awas360_mobile/widgets/primary_button/primary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class VerifyCodeScreen extends StatefulWidget {
  @override
  _VerifyCodeScreenState createState() => _VerifyCodeScreenState();
}

class _VerifyCodeScreenState extends State<VerifyCodeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              headerContent(context),
              body(context),
              formNumber(context),
            ],
          ),
        ),
        backgroundColor: Colors.white);
  }

  Widget headerContent(BuildContext context) {
    return Column(
      children: [
        Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            ClipPath(
              child: Container(
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                height: 308,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    AppColors.primaryLightkColor,
                    AppColors.primaryLightkColor
                  ]),
                ),
              ),
              clipper: CurveShape(),
            ),
            ClipPath(
              child: Container(
                  padding:
                      EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  height: 300,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      AppColors.gradientStart,
                      AppColors.gradientEnd
                    ]),
                  ),
                  child: Container(
                      padding: EdgeInsets.only(bottom: 50),
                      child: Image.asset('assets/vector/vector_verify2.png'))),
              clipper: CurveShape(),
            ),
          ],
        )
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: [
        Text('Verifikasi Kode',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 28,
            )),
        SizedBox(
          height: 4,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Text(
              'Masukan kode OTP yang kami kirimkan melalui SMS pada nomer berikut',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, color: Colors.grey)),
        ),
        SizedBox(
          height: 4,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Text('+62081234567890',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.deepOrange)),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }

  Widget formNumber(BuildContext context) {
    return FormBuilder(
        child: Column(
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Container(
            width: 35,
            child: FormBuilderTextField(
                cursorColor: AppColors.primaryLightkColor,
                attribute: 'number_phone',
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                maxLength: 1,
                style: TextStyle(
                    letterSpacing: 4,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                decoration: InputDecoration(
                  counterText: '',
                  hintStyle: TextStyle(
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                )),
          ),
          Container(
            width: 35,
            margin: EdgeInsets.only(left: 14),
            child: FormBuilderTextField(
                cursorColor: AppColors.primaryLightkColor,
                attribute: 'number_phone',
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                maxLength: 1,
                style: TextStyle(
                    letterSpacing: 4,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                decoration: InputDecoration(
                  counterText: '',
                  hintStyle: TextStyle(
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                )),
          ),
          Container(
            width: 35,
            margin: EdgeInsets.only(left: 14),
            child: FormBuilderTextField(
                cursorColor: AppColors.primaryLightkColor,
                attribute: 'number_phone',
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                maxLength: 1,
                style: TextStyle(
                    letterSpacing: 4,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                decoration: InputDecoration(
                  counterText: '',
                  hintStyle: TextStyle(
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                )),
          ),
          Container(
            width: 35,
            margin: EdgeInsets.only(left: 14),
            child: FormBuilderTextField(
                cursorColor: AppColors.primaryLightkColor,
                attribute: 'number_phone',
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                maxLength: 1,
                style: TextStyle(
                    letterSpacing: 4,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                decoration: InputDecoration(
                  counterText: '',
                  hintStyle: TextStyle(
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                )),
          ),
          Container(
            width: 35,
            margin: EdgeInsets.only(left: 14),
            child: FormBuilderTextField(
                cursorColor: AppColors.primaryLightkColor,
                attribute: 'number_phone',
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                maxLength: 1,
                style: TextStyle(
                    letterSpacing: 4,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                decoration: InputDecoration(
                  counterText: '',
                  hintStyle: TextStyle(
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                )),
          ),
          Container(
            width: 35,
            margin: EdgeInsets.only(left: 14),
            child: FormBuilderTextField(
                cursorColor: AppColors.primaryLightkColor,
                attribute: 'number_phone',
                textAlign: TextAlign.center,
                keyboardType: TextInputType.number,
                maxLength: 1,
                style: TextStyle(
                    letterSpacing: 4,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 28),
                decoration: InputDecoration(
                  counterText: '',
                  hintStyle: TextStyle(
                      letterSpacing: 0,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                  border: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.primaryColor, width: 3),
                  ),
                )),
          ),
        ]),
        Container(
          margin: EdgeInsets.only(left: 100, right: 100, top: 60, bottom: 8),
          child: PrimaryButton(
              text: 'VERIFIKASI',
              textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              textColor: Colors.white,
              color: AppColors.primaryColor,
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EmergencyCallScreen()))),
        ),
      ],
    ));
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(mainAxisSize: MainAxisSize.min, children: [
            Image.asset(
              'assets/icon/ic_verified.png',
              height: 120,
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              child: Text('VERIFIKASI BERHASIL',
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(fontSize: 18, color: AppColors.primaryColor)),
            ),
          ]),
        );
      },
    );
  }
}
