import 'package:awas360_mobile/screens/theme/curve_shape.dart';
import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/verify_screen/verify_code.dart';
import 'package:awas360_mobile/widgets/primary_button/primary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class VerifyNumberScreen extends StatefulWidget {
  @override
  _VerifyNumberScreenState createState() => _VerifyNumberScreenState();
}

class _VerifyNumberScreenState extends State<VerifyNumberScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              headerContent(context),
              body(context),
              formNumber(context),
            ],
          ),
        ),
        backgroundColor: Colors.white);
  }

  Widget headerContent(BuildContext context) {
    return Column(
      children: [
        Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            ClipPath(
              child: Container(
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                height: 308,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                    AppColors.primaryLightkColor,
                    AppColors.primaryLightkColor
                  ]),
                ),
              ),
              clipper: CurveShape(),
            ),
            ClipPath(
              child: Container(
                  padding:
                      EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  height: 300,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      AppColors.gradientStart,
                      AppColors.gradientEnd
                    ]),
                  ),
                  child: Container(
                      padding: EdgeInsets.only(bottom: 50),
                      child: Image.asset('assets/vector/vector_verify1.png'))),
              clipper: CurveShape(),
            ),
          ],
        )
      ],
    );
  }

  Widget body(BuildContext context) {
    return Column(
      children: [
        Text('Verifikasi Nomer Anda',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 28,
            )),
        SizedBox(
          height: 4,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Text(
              'Harap masukkan nomor telepon Anda untuk menerima kode verifikasi. Pastikan nomer yang anda masukan benar.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18, color: Colors.grey)),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }

  Widget formNumber(BuildContext context) {
    return FormBuilder(
        child: Column(
      children: [
        Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 4),
                    child: Text('+62',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 24)),
                  ),
                  Container(
                    color: AppColors.primaryColor,
                    width: 40,
                    height: 3,
                  )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.7,
                padding: EdgeInsets.only(left: 20, bottom: 1.5),
                child: FormBuilderTextField(
                    cursorColor: AppColors.primaryLightkColor,
                    attribute: 'number_phone',
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    maxLength: 13,
                    style: TextStyle(
                        letterSpacing: 4,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 22),
                    decoration: InputDecoration(
                      counterText: '',
                      hintText: 'Nomer Handphone',
                      hintStyle: TextStyle(
                          letterSpacing: 0,
                          fontWeight: FontWeight.normal,
                          color: Colors.grey),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.primaryColor, width: 3),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.primaryColor, width: 3),
                      ),
                      border: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: AppColors.primaryColor, width: 3),
                      ),
                    )),
              ),
            ]),
        Container(
          margin: EdgeInsets.only(left: 100, right: 100, top: 60, bottom: 8),
          child: PrimaryButton(
            text: 'KIRIM',
            textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            textColor: Colors.white,
            color: AppColors.primaryColor,
            onPressed: () => Navigator.push(context,
                MaterialPageRoute(builder: (context) => VerifyCodeScreen())),
          ),
        ),
      ],
    ));
  }
}
