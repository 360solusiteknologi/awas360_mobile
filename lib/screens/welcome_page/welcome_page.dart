import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/verify_screen/verify_number.dart';
import 'package:awas360_mobile/widgets/primary_button/primary_button.dart';
import 'package:flutter/material.dart';

class WelcomePageScreen extends StatefulWidget {
  @override
  _WelcomePageScreenState createState() => _WelcomePageScreenState();
}

class _WelcomePageScreenState extends State<WelcomePageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(context),
      backgroundColor: Colors.white,
    );
  }

  Widget body(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 100,
                ),
                Row(
                  children: [
                    Text(
                      'AWAS',
                      style: TextStyle(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                        fontSize: 34,
                      ),
                    ),
                    Text(
                      '360',
                      style: TextStyle(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 3,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.yellow,
                ),
                SizedBox(
                  height: 30,
                ),
                Text('Selamat datang di aplikasi AWAS.',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )),
                SizedBox(
                  height: 10,
                ),
                Text('Mohon untuk mengikuti semua aturan aplikasi.',
                    style: TextStyle(
                      fontSize: 16,
                    )),
                SizedBox(
                  height: 4,
                ),
                Text(
                  'Untuk menikmati layanan penuh dari AWAS, dimohon untuk mengaktifkan :',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  '1. Aktifkan GPS Smartphone',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  '2. Aktifkan Bluetooth',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  '3. Pastikan jaringan internet Anda aktif mobile data / WIFI ',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
            Expanded(
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                  margin: EdgeInsets.only(left: 40, right: 40, bottom: 40),
                  child: PrimaryButton(
                    text: 'MENGERTI',
                    textStyle:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    textColor: Colors.white,
                    color: AppColors.primaryColor,
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => VerifyNumberScreen())),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
