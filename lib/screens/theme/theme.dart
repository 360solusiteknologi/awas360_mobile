import 'package:flutter/material.dart';

final ThemeData appThemeData = ThemeData(
  brightness: Brightness.light,
  primarySwatch: AppColors.accentColor,
  // primaryColor: AppColors.primaryColor[500],
  primaryColor: AppColors.primaryColor,
  primaryColorBrightness: Brightness.light,
  accentColor: AppColors.primaryAssentColor,
  backgroundColor: Colors.blueGrey[50],
  scaffoldBackgroundColor: Colors.blueGrey[50],
  primaryIconTheme: IconThemeData(color: Colors.white),
  iconTheme: IconThemeData(color: Colors.white),
  primaryTextTheme: TextTheme(headline1: TextStyle(color: Colors.white)),
);

class AppColors {
  AppColors._(); // this basically makes it so you can instantiate this class

  // static const Color loginGradientStart = primaryColor;
  // static const Color loginGradientEnd = accentColor;

  static const primaryDarkColor = Color(0xFF355163);
  static const primaryColor = Color(0xFF43658B);
  static const primaryAssentColor = Color(0xFF577BA4);
  static const primaryLightkColor = Color(0xFFAFC7E3);

  static const blueForm1 = Color(0xFF3190C9);
  static const blueForm2 = Color(0xFF6BABD2);
  static const orangeTheme = Color(0xFFFFA372);

  static const gradientStart = Color(0xFF43658B);
  static const gradientEnd = Color(0xFF51A0D2);

  // static const MaterialColor primaryColor = MaterialColor(
  //   0xFF64B2CD,
  //   <int, Color>{
  //     50: Color(0xFFECF6F9),
  //     100: Color(0xFFD1E8F0),
  //     200: Color(0xFFB2D9E6),
  //     300: Color(0xFF93C9DC),
  //     400: Color(0xFF7BBED5),
  //     500: Color(0xFF64B2CD),
  //     600: Color(0xFF5CABC8),
  //     700: Color(0xFF52A2C1),
  //     800: Color(0xFF4899BA),
  //     900: Color(0xFF368AAE),
  //   },
  // );

  static const MaterialColor accentColor = MaterialColor(
    0xFF3C70A4,
    <int, Color>{
      50: Color(0xFFE8EEF4),
      100: Color(0xFFC5D4E4),
      200: Color(0xFF9EB8D2),
      300: Color(0xFF779BBF),
      400: Color(0xFF5985B2),
      500: Color(0xFF3C70A4),
      600: Color(0xFF36689C),
      700: Color(0xFF2E5D92),
      800: Color(0xFF275389),
      900: Color(0xFF1A4178),
    },
  );
}
