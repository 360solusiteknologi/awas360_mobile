import 'dart:ui';

import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/widgets/primary_button/primary_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'domicile_address.dart';

class EmergencyCallScreen extends StatefulWidget {
  @override
  _EmergencyCallScreenState createState() => _EmergencyCallScreenState();
}

class _EmergencyCallScreenState extends State<EmergencyCallScreen> {
  final items = List<String>.generate(3, (i) => "items");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: Container(
          margin: EdgeInsets.only(bottom: 20, left: 25),
          alignment: Alignment.bottomCenter,
          child: FloatingActionButton(
            elevation: 3,
            backgroundColor: Colors.green,
            splashColor: AppColors.primaryColor,
            onPressed: () => _showDialog(),
            child: Icon(Icons.add),
          ),
        ),
        body: bodyBackground(context));
  }

  Widget headerWizard(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              margin: EdgeInsets.only(left: 22, top: 34),
              child: Icon(
                FontAwesomeIcons.arrowLeft,
                size: 22,
              )),
          Row(
            children: [
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 22, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 2, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white30,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 2, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white30,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 2, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white30,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 22, right: 22, top: 34),
              child: Icon(
                FontAwesomeIcons.infoCircle,
                size: 22,
              )),
        ],
      ),
    );
  }

  Widget bodyBackground(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                tileMode: TileMode.clamp,
                colors: [AppColors.gradientStart, AppColors.gradientEnd]),
          ),
        ),
        headerWizard(context),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Center(
              child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(43, 62, 43, 50),
                decoration: BoxDecoration(
                    color: AppColors.blueForm1,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              ),
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(30, 71, 30, 50),
                decoration: BoxDecoration(
                    color: AppColors.blueForm2,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              ),
              SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height / 1.2,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.fromLTRB(20, 80, 20, 30),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: bodyContent(context),
                ),
              ),
            ],
          )),
        ),
      ],
    );
  }

  Widget bodyContent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text('Panggilan Darurat',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: AppColors.primaryColor)),
        ),
        SizedBox(
          height: 8,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 40),
          child: Text(
              'Mohon masukan nomer handphone yang akan sistem hubungi secara otomatis dalam keadaan darurat.',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16, color: Colors.grey)),
        ),
        SizedBox(
          height: 8,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Text('(Minimal 2 nomer)',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16, color: Colors.grey)),
        ),
        formNumber(context),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 2,
              color: AppColors.primaryColor,
            ),
            Container(
              child: FlatButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DomicileAddressScreen())),
                  child: Text('Lanjut',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: AppColors.primaryColor))),
            ),
          ],
        )),
      ],
    );
  }

  Widget formNumber(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height / 2,
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context, index) {
            return Dismissible(
              onDismissed: (DismissDirection direction) {
                setState(() {
                  items.removeAt(index);
                });
              },
              secondaryBackground: Container(
                child: Container(
                  margin: EdgeInsets.only(right: 12),
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.delete,
                    size: 30,
                  ),
                ),
                color: Colors.red,
              ),
              background: Container(),
              child: Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                child: Card(
                  elevation: 3,
                  child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomCenter,
                            tileMode: TileMode.clamp,
                            colors: [
                              AppColors.gradientStart,
                              AppColors.gradientEnd
                            ]),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: ListTile(
                        title: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Budi Santoso',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                                Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.redAccent,
                                      radius: 9,
                                      child: IconButton(
                                        padding: EdgeInsets.zero,
                                        icon: Icon(
                                          Icons.phone,
                                          size: 12,
                                        ),
                                        color: Colors.white,
                                        onPressed: () {},
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 4),
                                      child: Text(
                                        'Emergency Call',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 13),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Container(
                              color: Colors.white24,
                              height: 1,
                              width: 150,
                              margin: EdgeInsets.only(top: 4, bottom: 4),
                            ),
                          ],
                        ),
                        subtitle: Text(
                          '0814-2324-8421',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                      )),
                ),
              ),
              key: UniqueKey(),
              direction: DismissDirection.endToStart,
            );
          },
        ));
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(mainAxisSize: MainAxisSize.min, children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 40),
              child: Text('Tambah Panggilan Darurat',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold)),
            ),
            SizedBox(
              height: 14,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
              child: FormBuilderTextField(
                  cursorColor: AppColors.primaryLightkColor,
                  attribute: 'name',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                  decoration: InputDecoration(
                    hintText: 'Nama',
                    hintStyle: TextStyle(color: Colors.grey),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.primaryColor, width: 1),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.primaryColor, width: 1),
                    ),
                    border: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.primaryColor, width: 1),
                    ),
                  )),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: FormBuilderTextField(
                  cursorColor: AppColors.primaryLightkColor,
                  attribute: 'name',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                  decoration: InputDecoration(
                    hintText: 'Nomer Handphone',
                    hintStyle: TextStyle(color: Colors.grey),
                    enabledBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.primaryColor, width: 1),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.primaryColor, width: 1),
                    ),
                    border: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: AppColors.primaryColor, width: 1),
                    ),
                  )),
            ),
            Container(
              margin: EdgeInsets.only(left: 50, right: 50, top: 40),
              child: PrimaryButton(
                text: 'OKEY',
                textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                textColor: Colors.white,
                color: AppColors.primaryColor,
                onPressed: () => Navigator.pop(context),
              ),
            ),
          ]),
        );
      },
    );
  }
}
