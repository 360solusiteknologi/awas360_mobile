import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'medical_history.dart';

class DomicileAddressScreen extends StatefulWidget {
  @override
  _DomicileAddressScreenState createState() => _DomicileAddressScreenState();
}

class _DomicileAddressScreenState extends State<DomicileAddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: bodyBackground(context));
  }

  Widget headerWizard(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              margin: EdgeInsets.only(left: 22, top: 34),
              child: Icon(
                FontAwesomeIcons.arrowLeft,
                size: 22,
              )),
          Row(
            children: [
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 22, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 2, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 2, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white30,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
              Container(
                height: 7,
                width: 50,
                margin: EdgeInsets.only(left: 2, top: 34),
                decoration: BoxDecoration(
                    color: Colors.white30,
                    borderRadius: BorderRadius.all(Radius.circular(2))),
              ),
            ],
          ),
          Container(
              margin: EdgeInsets.only(left: 22, right: 22, top: 34),
              child: Icon(
                FontAwesomeIcons.infoCircle,
                size: 22,
              )),
        ],
      ),
    );
  }

  Widget bodyBackground(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                tileMode: TileMode.clamp,
                colors: [AppColors.gradientStart, AppColors.gradientEnd]),
          ),
        ),
        headerWizard(context),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Center(
              child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(43, 62, 43, 50),
                decoration: BoxDecoration(
                    color: AppColors.blueForm1,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              ),
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(30, 71, 30, 50),
                decoration: BoxDecoration(
                    color: AppColors.blueForm2,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
              ),
              SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height / 1.2,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.fromLTRB(20, 80, 20, 30),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: bodyContent(context),
                ),
              ),
            ],
          )),
        ),
      ],
    );
  }

  Widget bodyContent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text('Alamat Domisili',
                style: TextStyle(
                    fontSize: 20,
                    color: AppColors.primaryColor,
                    fontWeight: FontWeight.bold))),
        SizedBox(
          height: 8,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 40),
          child: Text(
              'Masukan alamat rumah sesuai dengan domisili tempat anda tinggal saat ini.',
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 16, color: Colors.grey)),
        ),
        formNumber(context),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 2,
              color: AppColors.primaryColor,
            ),
            Container(
              child: FlatButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MedicalHistoryScreen())),
                  child: Text('Lanjut',
                      style: TextStyle(
                          fontSize: 18, color: AppColors.primaryColor))),
            ),
          ],
        )),
      ],
    );
  }

  Widget formNumber(BuildContext context) {
    return FormBuilder(
        child: Column(
      children: [
        SizedBox(
          height: 30,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: FormBuilderDropdown(
              attribute: "province",
              hint: Text(
                'Provinsi',
                style: TextStyle(color: Colors.grey),
              ),
              validators: [FormBuilderValidators.required()],
              items: ['Jawa Timur', 'Jawa Tengah', 'Jawa Barat']
                  .map((gender) =>
                      DropdownMenuItem(value: gender, child: Text("$gender")))
                  .toList(),
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                border: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
              )),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: FormBuilderDropdown(
              attribute: "kota",
              hint: Text(
                'Kota',
                style: TextStyle(color: Colors.grey),
              ),
              validators: [FormBuilderValidators.required()],
              items: ['Bandung', 'Jakarta', 'Surabaya']
                  .map((gender) =>
                      DropdownMenuItem(value: gender, child: Text("$gender")))
                  .toList(),
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                border: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
              )),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: FormBuilderDropdown(
              attribute: "kecamatan",
              hint: Text(
                'Kecamatan',
                style: TextStyle(color: Colors.grey),
              ),
              validators: [FormBuilderValidators.required()],
              items: ['Rancasari', 'Gede Bage', 'Jatiasih']
                  .map((gender) =>
                      DropdownMenuItem(value: gender, child: Text("$gender")))
                  .toList(),
              decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                border: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
              )),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: FormBuilderTextField(
              cursorColor: AppColors.primaryLightkColor,
              attribute: 'number_phone',
              style: TextStyle(color: Colors.black, fontSize: 16),
              decoration: InputDecoration(
                hintText: 'Kelurahan',
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                border: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
              )),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: FormBuilderTextField(
              cursorColor: AppColors.primaryLightkColor,
              attribute: 'number_phone',
              style: TextStyle(color: Colors.black, fontSize: 16),
              decoration: InputDecoration(
                hintText: 'Alamat Lengkap',
                hintStyle: TextStyle(color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
                border: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: AppColors.primaryColor, width: 1),
                ),
              )),
        ),
      ],
    ));
  }
}
