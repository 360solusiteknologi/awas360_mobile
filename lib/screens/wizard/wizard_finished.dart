import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/verify_screen/verify_number.dart';
import 'package:awas360_mobile/widgets/primary_button/primary_button.dart';
import 'package:flutter/material.dart';

class WizardFinishedScreen extends StatefulWidget {
  @override
  _WizardFinishedScreenState createState() => _WizardFinishedScreenState();
}

class _WizardFinishedScreenState extends State<WizardFinishedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(context),
      backgroundColor: Colors.white,
    );
  }

  Widget body(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 100,
                ),
                Row(
                  children: [
                    Text(
                      'AWAS',
                      style: TextStyle(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                        fontSize: 34,
                      ),
                    ),
                    Text(
                      '360',
                      style: TextStyle(
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic,
                        fontSize: 17,
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 3,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.yellow,
                ),
                SizedBox(
                  height: 30,
                ),
                Text('Selamat!',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    )),
                SizedBox(
                  height: 10,
                ),
                Text(
                    'Proses instalasi dan pelangkapan data diri dan kendaraan anda di aplikasi AWAS sudah berhasil dan tersimpan di sistem kami. ',
                    style: TextStyle(
                      fontSize: 16,
                    )),
                SizedBox(
                  height: 4,
                ),
                Text(
                  'Dimohon untuk melanjutkan ke proses Instalasi Device AWAS ketika alat sudah tiba di tangan customer.',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  'Pastikan anda menerima & memiliki 4 Device AWAS untuk melakukan pairing pada kendaraan anda.',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
            Expanded(
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                  margin: EdgeInsets.only(left: 40, right: 40, bottom: 40),
                  child: PrimaryButton(
                    text: 'MENGERTI',
                    textStyle:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    textColor: Colors.white,
                    color: AppColors.primaryColor,
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => VerifyNumberScreen())),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
