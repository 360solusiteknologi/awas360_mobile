import 'package:awas360_mobile/screens/theme/theme.dart';
import 'package:awas360_mobile/screens/walkthrough/walkthrough_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'AWAS360 MOBILE',
        theme: appThemeData,
        home: WalkthroughScreen());
  }
}
