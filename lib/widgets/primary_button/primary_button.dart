import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final String text;
  final TextStyle textStyle;
  final String pathIMG;
  final GestureTapCallback onPressed;
  final IconData icon;

  PrimaryButton(
      {@required this.text,
      @required this.onPressed,
      this.color,
      this.textColor,
      this.icon,
      this.pathIMG, this.textStyle});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      color: (color ?? Theme.of(context).accentColor),
      textColor: (textColor ?? Theme.of(context).accentTextTheme.button.color),
      elevation: 5,
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 15.0,
          horizontal: 15.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buttonContent(),
        ),
      ),
      onPressed: onPressed,
    );
  }

  List<dynamic> _buttonContent() {
    if (icon == null && pathIMG == null) {
      return <Widget>[
        Text(text, style: textStyle),
      ];
    } else {
      if (pathIMG == null) {
        return <Widget>[
          Icon(
            icon,
            color: Colors.white,
            size: 18,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              text,
              style: textStyle,
            ),
          ),
        ];
      } else {
        return <Widget>[
          Image.asset(
            pathIMG,
            scale: 20,
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              text,
            ),
          ),
        ];
      }
    }
  }
}
